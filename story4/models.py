from django.db import models
from django import forms
from datetime import date
import datetime
# Create your models here.
class Schedule(models.Model):
    date = models.DateField()
    time = models.TimeField()
    activity = models.CharField(max_length=250)
    place = models.CharField(max_length=250)
