from django import forms
from .models import Schedule
from datetime import datetime
import datetime


class FormSubmission(forms.ModelForm):
    date = forms.DateTimeField(widget=forms.DateInput(attrs={'type':'date','class':'field'}),required=True)
    time = forms.CharField(widget=forms.TextInput(attrs={'type':'time'}),required=True)
    activity = forms.CharField(widget=forms.TextInput(attrs={'type':'text'}),required=True)
    place = forms.CharField(widget=forms.TextInput(attrs={'type':'text'}),required=True)
    
    class Meta:
        model = Schedule
        fields = ('date','time','activity','place')
