from django.shortcuts import render
from .models import Schedule
from .forms import *

# Create your views here.
def home(request):
    html = "home.html"
    return render(request, html)

def formTamu(request):
    html = "form tamu.html"
    return render(request, html)

def myProfile(request):
    html = "my profile.html"
    return render(request, html)
     
def Submission(request):
    all_schedule = Schedule.objects.all()
    html = "Submission.html"
    return render(request, html, {'Schedule': all_schedule})
    
def jadwal(request):
    all_schedule = Schedule.objects.all()
    if (request.method == "POST"):
        form = FormSubmission(request.POST or None)
        if form.is_valid():
            date = request.POST["date"]
            time = request.POST[ "time"]
            activity = request.POST["activity"]
            place = request.POST["place"]
            post = Schedule(date=date, time=time, activity=activity, place=place)
            post.save()
    else:
        
        form = FormSubmission()
    return render(request, 'Submission.html', {'Schedule': all_schedule,'form':form})

def activity(request):
    activity_list = Schedule.objects.all()
    return render(request, 'Submission.html', {'Activity':activity_list})
